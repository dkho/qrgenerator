import pandas as pd
import pyqrcode
import openpyxl
import qrcode
import base64
import csv
import xlrd
import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import askopenfile
from openpyxl import Workbook
from PIL import Image, ImageDraw, ImageFont, ImageTk #Save and display the image
import os 


#pour exécuter le code: [python testQR.py]

root = tk.Tk()

canvas = tk.Canvas(root, width=600, height=300)
canvas.grid(columnspan=3, rowspan=3)

#logo
logo = Image.open('logo.png')
logo = ImageTk.PhotoImage(logo)
logo_label = tk.Label(image=logo)
logo_label.image = logo
logo_label.grid(column=1, row=0)

#instructions
instructions = tk.Label(root, text='Select a file to generate QR code for all the users', font='Raleway')
instructions.grid(columnspan=3, column=0, row=1)
label_file = ttk.Label()

#def open_file():
#    file = askopenfile(initialdir="/", title="Select File", filetypes=(("excel files", "*.xls*"), ("csv files", "*.csv*")))
#    wb = Workbook()
#    wb = xlrd.open_workbook(file)
#    wb.save(filename='qrdata.xls')
#    return None


def createQRCode():
    #file_path = label_file["text"]
    i = 1
    qrFolder ='savefolder/qrcodes/'
    boxPixels = 15
    try:
        #excel_filename = r"{}".format(file_path)
        df = pd.DataFrame(pd.read_excel('data.xls', converters={'Internal Reference': lambda x: str(x)}))
        new_df = df.filter(['Name', 'Internal Reference'])
    except ValueError:
        tk.messagebox.showerror("Information", "The file you have chosen is invalid")
        return None
    except FileNotFoundError:
        tk.messagebox.error("Information", f"No such file as {file_path}")
        return None


    #fonts to be used for the caption in the generated QR image
    #if the font exists in the fonts folder, then it can be changed or else, you have to add the font that you desire
    fontFile = 'fonts/Helvetica.ttf'
    fontName = 'Helvetica.ttf'

    #Make a folder for saved QRcodes
    if not os.path.exists(qrFolder):
        os.makedirs(qrFolder)


    for i, values in new_df.iterrows():
        name = values["Name"]
        id = values["Internal Reference"]

        data = f'''

        {id} \n
        '''

        # Create qr code instance
        qr = qrcode.QRCode(border=10)

        # Add data
        qr.box_size = int(boxPixels)+2
        qr.add_data(values["Internal Reference"]) #Add the data from column 1 of the CSV
        qr.make(fit=True)

        # Create an image from the QR Code instance
        image = qr.make_image()


        #image = pyqrcode.create(data)

        #Add caption/text to the generated QR code
        #if doCaption:
        draw = ImageDraw.Draw(image)
        QRwidth, QRheight = image.size
        print(image.size)
        fontSize = 1 #starting font size

        #Get the caption from a certain column in the excel file
        qrLabel = name
        qrLabel2 = id

        image_fraction =0.80 # portion of image width you want text width to be
        fontHeightMax = qr.border * qr.box_size - 120 #Maximum height for captions
        captionX = 0 #Where to start the caption X coordinate
        captionY = 550 #Where to start the caption y coordinate

        captionX2 = 0 #Where to start the caption X coordinate
        captionY2 = 620 #Where to start the caption y coordinate
        #print('Font height max is set to: ' + str(fontHeightMax))
        font = ImageFont.truetype(fontFile, fontSize)
        while font.getsize(qrLabel)[0] < image_fraction*QRwidth and font.getsize(qrLabel)[1] < fontHeightMax:
            fontSize += 1
            font = ImageFont.truetype(fontFile, fontSize)
        captionX = int(QRwidth - font.getsize(qrLabel)[0]) / 2 #Center the label
        print('Offset: ' + str(captionX))
        draw.text((captionX, captionY), qrLabel, font=font)

        while font.getsize(qrLabel2)[0] < image_fraction*QRwidth and font.getsize(qrLabel2)[1] < fontHeightMax:
            fontSize += 1
            font = ImageFont.truetype(fontFile, fontSize)
        captionX2 = int(QRwidth - font.getsize(qrLabel2)[0]) /2 #Center the label
        print('Offset: ' + str(captionX2))
        draw.text((captionX2, captionY2), qrLabel2, font=font)

        #image.svg("qrcode{}.svg".format(i), scale=5)
        image.save(qrFolder + "qrcode{}.jpg".format(i))

        #--------------------------------------------------------------------------------------------
        #the following code enables us to resize the generated image to our desired pixel size
        #the code will determine the width of the image automatically based on the g=height of the image
        #if yopu want to set everything manually, just chaneg baseheight and wsize to your desired value and remove hpercent
        #--------------------------------------------------------------------------------------------
        #baseheight = 600
        #img = Image.open(qrFolder + "qrcode{}.jpg".format(i))
        #hpercent = (baseheight / float(img.size[1]))
        #wsize = QRwidth
        #img = img.resize((wsize, baseheight), Image.ANTIALIAS)
        #img.save(qrFolder + 'resized_image.jpg'.format(i))


#select file button
#select_file_text = tk.StringVar()
#select_file_btn = tk.Button(root, textvariable=select_file_text, command=lambda:open_file(), font="Raleway",
#                            bg="#20bebe", fg="white", height=2, width=15)
#select_file_text.set("Select File")
#select_file_btn.grid(column=1, row=2)

#generate code button
button2_text = tk.StringVar()
button2 = tk.Button(root, textvariable=button2_text, command=lambda:createQRCode(), font="Raleway",
                            bg="#20bebe", fg="white", height=2, width=15)
button2_text.set("Generate QR Code")
button2.grid(column=1, row=6)

root.mainloop()

#createQRCode()