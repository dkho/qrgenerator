# This file contains the instructions on how to use the QR Generator

It is recommended to use this application alongside the lcc_members module developped by Lokavaluto that in available 
in Odoo. 

After you have unzipped the folder that this application is in, in order to find the executable file of this programme, 
click on the dist folder and you will find an application named QRGenerator. Do not move the executable anywhere as some problems might occur and the application will not work.

To export the data from Odoo, select export data as Excel in the lcc_members module in Odoo and select the columns needed.
## ATTENTION : It is imperative to have the two column, Name and Internal Reference as Name contains the name while Internal Reference contain the ID of the members

## It is also possible to use it with data from another source but you need two column named 'Name' and 'Internal Reference'

## Saved the name of the file containing the data as data.xls

The generated images will be saved in a folder named 'savefolder'

In case you wish to change anything in the source code, the code has enough comments to guide you and after changing any code, you have to regenerate the executable file in order for the application to register the new changes

## Run this command to generate the executable file : pyinstaller --onefile QRGenerator.py
